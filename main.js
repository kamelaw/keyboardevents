//For this assessment, you will create a simple web page that moves a 
//box in response to keydown events.



//addEventListener keydown example
//This example logs the KeyboardEvent.code value whenever you press down a key.


//Initialize two new global variables outside the function logKey:
let boxTop = 200;
let boxLeft = 200;


document.addEventListener('keydown', logKey);


//add code to the logKey function so that the left and right cursor 
//keys modify the "left" style attribute of the box.
//You should now be able to move the box around the page using your arrow keys.
//Finally, replace the boring gray box with an image.

function logKey(event) {
  if(event.key == "ArrowRight") {
    boxLeft += 10
  }

  else if(event.key == "ArrowDown") {
    boxTop += 10
  }

  else if(event.key == "ArrowLeft") {
    boxLeft -= 10
  
  }

  else if(event.key == "ArrowUp") {
    boxTop -= 10
  }

  //log.textContent += ` ${event.code}`;

  //useful way to get access to a specific element quickly.
  document.getElementById("box").style.top = boxTop + "px";
  document.getElementById("box").style.left = boxLeft + "px";
  

}
